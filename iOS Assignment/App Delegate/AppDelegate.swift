//
//  AppDelegate.swift
//  iOS Assignment
//
//  Created by Apple on 23/03/22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window:UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        /* set previously selected language*/
        Bundle.setLanguage(lang: Bundle.getLanguage())
        return true
    }

}

