//
//  Connectivity.swift
//  iOS Assignment
//
//  Created by Apple on 23/03/22.
//

import Foundation
import Alamofire

class Connectivity {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
