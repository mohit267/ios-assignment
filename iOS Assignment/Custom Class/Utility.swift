//
//  Utility.swift
//  iOS Assignment
//
//  Created by Apple on 23/03/22.
//

import Foundation
import NotificationBannerSwift
import CoreLocation

var notificationBanner: FloatingNotificationBanner?

func showBanner(msg : String, style : BannerStyle) {
    notificationBanner?.dismiss()
    notificationBanner = FloatingNotificationBanner(title: "",
                                                    subtitle: msg,
                                                    style: style)
    
    notificationBanner?.onTap = {
        notificationBanner?.dismiss()
    }
    notificationBanner?.show()
}

func checkLocationPermission(_ completionHandler: @escaping ((Bool) -> Void)) {
    let manager = CLLocationManager()
    
    if CLLocationManager.locationServicesEnabled() {
        switch manager.authorizationStatus {
            case .restricted, .denied:
                completionHandler(false)
                break
            case .notDetermined, .authorizedAlways, .authorizedWhenInUse:
                completionHandler(true)
                break
            default:
                completionHandler(false)
                break
        }
    } else {
        completionHandler(false)
    }
}

func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
