//
//  ViewController.swift
//  iOS Assignment
//
//  Created by Apple on 23/03/22.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {
    
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblTemp: UILabel!
    @IBOutlet weak var lblWindSpeed: UILabel!
    @IBOutlet weak var imageWeather: UIImageView!
    @IBOutlet weak var lblWindDegree: UILabel!
    @IBOutlet weak var lblWindDir: UILabel!
    @IBOutlet weak var lblHumidity: UILabel!
    @IBOutlet weak var dataView: UIView!
    @IBOutlet weak var lblCurrentLanguage: UILabel!
    @IBOutlet weak var btnChangeLanguage: UIButton!
    @IBOutlet weak var lblDarkMode: UILabel!
    @IBOutlet weak var switchDarkMode: UISwitch!
    
    let locationManager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpView()
    }
    
    func setUpView() {
        self.dataView.isHidden = true
        /* check dark mode setting */
        if isDarkModeEnable {
            overrideUserInterfaceStyle = .dark
            self.switchDarkMode.isOn = true
        } else {
            overrideUserInterfaceStyle = .light
            self.switchDarkMode.isOn = false
        }
        self.lblCurrentLanguage.text = "current_language".localized() + " \(Bundle.getLanguage())"
        self.btnChangeLanguage.setTitle("change_language".localized(), for: .normal)
        self.lblDarkMode.text = "dark_mode".localized()
        self.checkLocationAccess()
        self.getCurrentLocation()
    }
    
    func checkLocationAccess() {
        /* check if location permission is given, if not show alert */
        checkLocationPermission { hasPermission in
            if !hasPermission {
                DispatchQueue.main.async {
                    self.showAlertMessageWithTwoCompletion(alertTitle: "permission_denied".localized(), alertMsg: "permission_msg".localized(), btnOneTitle: "cancel".localized(), btnTwoTitle: "open_settings".localized()) { action in
                    } actionTwo: { action in
                        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                    }
                }
            }
        }
    }
    
    func getCurrentLocation() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            self.locationManager.startUpdatingLocation()
        }
    }

    //MARK: API Call
    
    func getWeatherData(latitude: Double, longitude: Double) {
        if Connectivity.isConnectedToInternet() {
            let location = "\(latitude),\(longitude)"
            var url: URL = URL(string: APIURL.Shared.currentWeatherURL)!
            url = url.appending("access_key", value: APIAccessKeys.WeatherStackAccessKey)
            url = url.appending("query", value: location)
            
            self.view.showProgressView()
            AlamofireAPICall(apiURL: url, resultType: CurrentWeatherResponse.self, method: .get) { weatherData in
                self.view.hideProgressView()
                if let success = weatherData.success, !success {
                    if let errorData = weatherData.error, let info = errorData.info {
                        showBanner(msg: info, style: .danger)
                    }
                } else {
                    if let currentWeatherData = weatherData.current {
                        if let temp = currentWeatherData.temperature {
                            self.lblTemp.text = "\(temp)° C"
                        }
                        if let humidity = currentWeatherData.humidity {
                            self.lblHumidity.text =  "humidity".localized() + ": \(humidity) %"
                        }
                        if let windSpeed = currentWeatherData.wind_speed {
                            self.lblWindSpeed.text = "wind".localized() +  ": \(windSpeed) km/h"
                        }
                        if let windDir = currentWeatherData.wind_dir {
                            self.lblWindDir.text =  "wind_direction".localized() + ": \(windDir)"
                        }
                        if let windDegree = currentWeatherData.wind_degree {
                            self.lblWindDegree.text = "wind_Degree".localized() + ": \(windDegree)"
                        }
                        if let weatherIcons = currentWeatherData.weather_icons, weatherIcons.count > 0 {
                            self.imageWeather.SetImage(url: weatherIcons[0])
                        }
                    }
                    if let locationData = weatherData.location {
                        var locationText = ""
                        if let name = locationData.name {
                            locationText = name
                        }
                        if let region = locationData.region {
                            if locationText.isValidString {
                                locationText = locationText + ", " + region
                            } else {
                                locationText = region
                            }
                        }
                        if let country = locationData.country {
                            if locationText.isValidString {
                                locationText = locationText + ", " + country
                            } else {
                                locationText = country
                            }
                        }
                        self.lblLocation.text = locationText
                    }
                    self.dataView.isHidden = false
                }
            }
        } else {
            showBanner(msg: "no_internet_msg".localized(), style: .danger)
        }
    }
    
    //MARK: IBAction Methods
    
    @IBAction func change_language_clicked(_ sender: Any) {
        /* change language */
        if Bundle.getLanguage() == "hi" {
            Bundle.setLanguage(lang: "en")
        } else {
            Bundle.setLanguage(lang: "hi")
        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "vc") as! ViewController
        let appDlg = UIApplication.shared.delegate as? AppDelegate
        appDlg?.window?.rootViewController = vc
    }
    
    @IBAction func dark_mode_changed(_ sender: UISwitch) {
        /* enable/disable dark mode */
        if sender.isOn {
            overrideUserInterfaceStyle = .dark
            USERDEFAULTS.set(true, forKey: UserDefKeys.IsDarkMode)
        } else {
            overrideUserInterfaceStyle = .light
            USERDEFAULTS.set(false, forKey: UserDefKeys.IsDarkMode)
        }
    }
}

extension ViewController : CLLocationManagerDelegate {
 
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location : CLLocationCoordinate2D = manager.location!.coordinate
        self.getWeatherData(latitude: location.latitude, longitude: location.longitude)
        self.locationManager.stopUpdatingLocation()
    }
}

