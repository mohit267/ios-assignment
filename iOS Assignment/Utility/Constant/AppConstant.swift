//
//  AppConstant.swift
//  iOS Assignment
//
//  Created by Apple on 23/03/22.
//

import Foundation

let USERDEFAULTS = UserDefaults.standard

struct APIAccessKeys {
    /* Access key for weather stack API */
    static let WeatherStackAccessKey = "da95fcab1f0f9ec9002a9948ae0f7e81"
}

struct UserDefKeys {
    static let AppLanguage = "app_lang"
    static let IsDarkMode = "dark_mode"
}

var isDarkModeEnable : Bool {
    if let isDarkMode = USERDEFAULTS.value(forKey: UserDefKeys.IsDarkMode) as? Bool {
        return isDarkMode
    }
    return true
}
