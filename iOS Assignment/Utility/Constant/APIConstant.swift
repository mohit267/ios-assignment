//
//  APIConstant.swift
//  iOS Assignment
//
//  Created by Apple on 23/03/22.
//

import Foundation

struct APIBaseURL {
    /* Base URLS for API */
    static let WeatherStackBaseURL = "http://api.weatherstack.com/"
}

struct WeatherStackEndPoint {
    /* End Point for API */
    static let Current = "current"
}

class APIURL {
    /* Final API URL */
    static let Shared = APIURL()
    
    let currentWeatherURL = APIBaseURL.WeatherStackBaseURL + WeatherStackEndPoint.Current
}
