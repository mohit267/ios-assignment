//
//  APIManager.swift
//  iOS Assignment
//
//  Created by Apple on 23/03/22.
//

import Foundation
import Alamofire

func AlamofireAPICall<T: Decodable>(apiURL : URL,
                            resultType: T.Type,
                            method : HTTPMethod,
                            completion: @escaping(T) -> Void) {
    
    Alamofire.request(apiURL, method: method).responseJSON { response  in
        print(response.value as Any)
        
        if response.response != nil {
            do {
                let decodedData = try JSONDecoder().decode(resultType, from: response.data!)
                completion(decodedData)
            } catch let error {
                print("Error while decoding data: ", error.localizedDescription)
            }
        }
    }
}
