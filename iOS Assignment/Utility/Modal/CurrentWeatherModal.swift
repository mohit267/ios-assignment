//
//  CurrentWeatherModal.swift
//  iOS Assignment
//
//  Created by Apple on 23/03/22.
//

import Foundation

struct CurrentWeatherResponse : Codable {
    let success: Bool?
    let error : Error?
    let request : Request?
    let location : Location?
    let current : Current?
    
    private enum CodingKeys: String, CodingKey {
        case success
        case error
        case request
        case location
        case current
    }
}

struct Error : Codable {
    let code : Int?
    let type : String?
    let info : String?
    
    private enum CodingKeys: String, CodingKey {
        case code
        case type
        case info
    }
}

struct Request : Codable {
    let type : String?
    let query : String?
    let language : String?
    let unit : String?
    
    enum CodingKeys: String, CodingKey {
        case type
        case query
        case language
        case unit
    }
}

struct Location : Codable {
    let name : String?
    let country : String?
    let region : String?
    let lat : String?
    let lon : String?
    let timezone_id : String?
    let localtime : String?
    let localtime_epoch : Int?
    let utc_offset : String?
    
    enum CodingKeys: String, CodingKey {
        case name
        case country
        case region
        case lat
        case lon
        case timezone_id
        case localtime
        case localtime_epoch
        case utc_offset
    }
}

struct Current : Codable {
    let observation_time : String?
    let temperature : Int?
    let weather_code : Int?
    let weather_icons : [String]?
    let weather_descriptions : [String]?
    let wind_speed : Int?
    let wind_degree : Int?
    let wind_dir : String?
    let pressure : Int?
    let precip : Int?
    let humidity : Int?
    let cloudcover : Int?
    let feelslike : Int?
    let uv_index : Int?
    let visibility : Int?
    let is_day : String?
    
    enum CodingKeys: String, CodingKey {
        case observation_time
        case temperature
        case weather_code
        case weather_icons
        case weather_descriptions
        case wind_speed
        case wind_degree
        case wind_dir
        case pressure
        case precip
        case humidity
        case cloudcover
        case feelslike
        case uv_index
        case visibility
        case is_day
    }
}
