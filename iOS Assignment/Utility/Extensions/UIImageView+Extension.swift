//
//  UIImageView+Extension.swift
//  iOS Assignment
//
//  Created by Apple on 23/03/22.
//

import Foundation
import UIKit
import Kingfisher

extension UIImageView {
    
    func SetImage(url: String) {
        let url = URL(string: url)
        self.kf.setImage(with: url)
    }
}
