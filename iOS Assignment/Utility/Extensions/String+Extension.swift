//
//  String+Extension.swift
//  iOS Assignment
//
//  Created by Apple on 23/03/22.
//

import Foundation

extension String {
    
    var isValidString :Bool{
        let str = self.trimmingCharacters(in: .whitespacesAndNewlines)
        return !str.isEmpty && str != "<null>"
    }
    
    func localized() -> String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.localizedBundle(), value: "", comment: "")
    }
}
