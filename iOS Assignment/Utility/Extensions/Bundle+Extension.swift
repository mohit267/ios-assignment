//
//  Bundle+Extension.swift
//  iOS Assignment
//
//  Created by Apple on 24/03/22.
//

import Foundation

extension Bundle {
    private static var bundle: Bundle!
    
    public static func localizedBundle() -> Bundle! {
        if bundle == nil {
            let appLang = USERDEFAULTS.string(forKey: UserDefKeys.AppLanguage) ?? "en"
            let path = Bundle.main.path(forResource: appLang, ofType: "lproj")
            bundle = Bundle(path: path!)
        }
        
        return bundle;
    }
    
    /* set language */
    public static func setLanguage(lang: String) {
        USERDEFAULTS.set(lang, forKey: UserDefKeys.AppLanguage)
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        bundle = Bundle(path: path!)
    }
    
    /* get current language*/
    public static func getLanguage() -> String {
        return USERDEFAULTS.string(forKey: UserDefKeys.AppLanguage) ?? "en"
    }
}
