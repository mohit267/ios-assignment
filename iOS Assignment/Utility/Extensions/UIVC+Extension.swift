//
//  UIVC+Extension.swift
//  iOS Assignment
//
//  Created by Apple on 23/03/22.
//

import Foundation
import UIKit

extension UIViewController {
    
    func showAlertMessageWithTwoCompletion (alertTitle: String, alertMsg : String, btnOneTitle : String, btnTwoTitle: String, actionOne:@escaping ((UIAlertAction) -> Void), actionTwo:@escaping ((UIAlertAction) -> Void)){
        let alert = UIAlertController(title: alertTitle, message: alertMsg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: btnOneTitle, style: .default, handler: actionOne))
        alert.addAction(UIAlertAction(title: btnTwoTitle, style: .default, handler: actionTwo))
        self.present(alert, animated: true, completion: {})
    }
}
