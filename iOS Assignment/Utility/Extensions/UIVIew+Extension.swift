//
//  UIVIew+Extension.swift
//  iOS Assignment
//
//  Created by Apple on 23/03/22.
//

import Foundation
import MBProgressHUD

extension UIView {
    
    func showProgressView() {
        let hud = MBProgressHUD.showAdded(to: self, animated: true)
        hud.isUserInteractionEnabled = false
    }
    
    func hideProgressView() {
        MBProgressHUD.hide(for: self, animated: true)
    }
}
